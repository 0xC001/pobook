# -*- coding: utf-8 -*-
# try something like
#@cache.action(time_expire=300, cache_model=cache.ram, quick='SVP')
@auth.requires_login()
def list():
    if not (request.vars.source == None):
        source = request.vars.source
        here = URL(args=request.args, vars=request.vars)
    else:
        redirect(URL('default', 'index'))


    if not (request.args(0) == None):
        
        url_add = URL('add', args=request.args, vars=dict(source=here))
        
        fields = [
        db.attachments.id,
        db.attachments.name,
        db.attachments.files
        ]

        grid = []
        
        details = {
                'vendor': None,
                'commitment': None,
                'invoice': None,
                'budget': None,
                'project': None
            }

        category = request.args(0)
        asset = request.args(1, cast=int)

        if category == 'vendor':
            query = db.attachments.vendor_id == asset
            details['vendor'] = db(db.vendors.id == asset).select()[0].name

        elif category == 'commitment':
            query = db.attachments.commitment_id == asset
            details['commitment'] = db(db.po.id == asset).select()[0].num

        elif category == 'invoice': 
            query = db.attachments.invoice_id == asset
            inv = db(db.invoices.id == asset).select()[0]
            details['invoice'] = inv.num
            details['vendor'] = db(db.vendors.id == inv.vendor_id).select()[0].name

        elif category == 'budget': 
            query = db.attachments.budget_id == asset
            details['budget'] = db(db.budget.id == asset).select()[0].comments

        elif category == 'project': 
            query = db.attachments.project_id == asset
            details['project'] = db(db.project.id == asset).select()[0].name
            
        else:
            redirect(URL('default', 'index'))
        
        links = [
            lambda row: A('Download',_class='button btn btn-default',_href='https://s3.ca-central-1.amazonaws.com/'+ bucket + '/'+ row.files),
            #lambda row: A('Edit',_class='button btn btn-default',_href=URL('edit', args=[row.id], vars=dict(source=here))),
            lambda row: A('Delete',_class='button btn btn-default',_href=URL('delete', args=[row.id, bucket, row.files], vars=dict(source=here)))
        ]
            
        grid = SQLFORM.grid(
            query,
            fields=fields,
            links=links,
            create=False,
            details=False,
            deletable=False,
            editable=False,
            paginate=10,
            orderby=[~db.attachments.updated_on],
            user_signature=False
        )
       

    else:
        redirect(URL('default', 'index'))

    return dict(list=grid, request=request, back=source, add=url_add, invoice=asset, details=details)



@auth.requires_login()
def add():
    if not (request.vars.source == None):
        source = request.vars.source
        here = URL(args=request.args, vars=request.vars)
    else:
        redirect(URL('default', 'index'))
    
    form = SQLFORM(db.attachments, fields = ['name', 'files'])
    
    form.vars.user_id = session.auth.user.id
    form.vars.tag = None
    form.vars.updated_on = request.now
    form.vars.updated_by = session.auth.user.id
    form.vars.locked_by = None
    
    if not (request.args(1, cast=int) == None):
        
        category = request.args(0)
        asset = request.args(1, cast=int)
        
        if category == 'vendor':
            form.vars.vendor_id = asset
            form.vars.commitment_id = None
            form.vars.invoice_id = None
            form.vars.budget_id = None
            form.vars.project_id = None
            
        elif category == 'commitment':
            form.vars.vendor_id = None
            form.vars.commitment_id = asset
            form.vars.invoice_id = None
            form.vars.budget_id = None
            form.vars.project_id = None
            
        elif category == 'invoice':
            form.vars.vendor_id = None
            form.vars.commitment_id = None
            form.vars.invoice_id = asset
            form.vars.budget_id = None
            form.vars.project_id = None
            
        elif category == 'budget':
            form.vars.vendor_id = None
            form.vars.commitment_id = None
            form.vars.invoice_id = None
            form.vars.budget_id = asset
            form.vars.project_id = None
            
        elif category == 'project':
            form.vars.vendor_id = None
            form.vars.commitment_id = None
            form.vars.invoice_id = None
            form.vars.budget_id = None
            form.vars.project_id = asset
            
        else:
            response.flash = 'form cannot be processed because of an internal server error'
    else:
        response.flash = 'Please go back to the page you were on previously, and try again'
    
    if form.process().accepted:
        redirect(source)

    elif form.errors:
        response.flash = 'form has errors'
        form.vars.files = None
    else:
        pass

    return dict(form=form, request=request, back=source, source=here)

@auth.requires_login()
def edit():
    if not (request.vars.source == None):
        source = request.vars.source
        here = URL(args=request.args, vars=request.vars)
    else:
        redirect(source)
    
    if not (request.args(0) == None):
        attachment_id = request.args(0, cast=int)
    else:
        redirect(source)
        
    files = db(db.attachments.id == attachment_id).select()
    
    url = URL('attachments', 'download', args=['db', files[0].files])
    
    form = SQLFORM(db.attachments, files[0], fields=['name', 'files'], upload=url)
    #form.vars.name = files[0].name
    #form.vars.files = files[0].files
    form.vars.updated_on = request.now
    form.vars.updated_by = session.auth.user.id
    form.vars.locked_user = None
    
    if form.accepts(request.vars, session):
        response.flash = 'form accepted'
        '''files[0].update_record(name=form.vars.name)
        files[0].update_record(files=form.vars.files)
        files[0].update_record(updated_on=form.vars.updated_on)
        files[0].update_record(updated_by=form.vars.updated_by)
        files[0].update_record(locked_user=form.vars.locked_user)
        db.commit()'''
        redirect(source)

    elif form.errors:
        response.flash = 'form has errors'
    else:
        pass

    return dict(form=form, request=request, back=source, source=here)


@auth.requires_login()
def download():
    redirect('https://s3.ca-central-1.amazonaws.com/'+'pobook/'+ request.args)
    return response.download(request, db)


@auth.requires_login()
def delete():
    '''
        /attachments/delete/id/bucket/file_name
    '''

    attachment_id = request.args(0,cast=int)
    bucket = request.args(1)
    file_name = request.args(2)
    
    
    no_error = s3delete(bucket, file_name)
    if no_error:
        db(db.attachments.id == attachment_id).delete()

        db.commit()

        redirect(request.vars.source)
    else:
        redirect('admin', 'error', args=[no_error])
    
    types = type(request.args(2))
    
    return dict(request=request, types=types)
