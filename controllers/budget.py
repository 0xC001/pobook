# -*- coding: utf-8 -*-
# try something like
#@cache.action(time_expire=300, cache_model=cache.ram, quick='SVP')
@auth.requires_login()
def list():
    """
    if not (request.vars.source == None):
        source = request.vars.source
        here = URL(args=request.args, vars=request.vars)
    else:
        redirect(URL('default', 'index'))
    """
    
    here = URL(args=request.args, vars=request.vars)
    
    fields = [db.budget.amount, db.budget.comments]
    
    if session.project in session.pm_projects:
        query = db.budget.project_id == session.project

        fields = [
            db.budget.id,
            db.budget.comments,
            db.budget.amount
        ]
        
        
        links = [
            #lambda row: A('Edit',_class='button btn btn-default',_href=URL('edit', args=[category, row.invoices.id], vars=dict(source=here))),
            lambda row: A('Files',_class='button btn btn-default',_href=URL('attachments', 'list', args=['budget',row.id], vars=dict(source=here))),
            lambda row: A('Delete',_class='button btn btn-default',_href=URL('delete', args=[row.id], vars=dict(source=here)))
        ]
        grid = SQLFORM.grid(
            query,
            fields = fields,
            links=links,
            create=False,
            details=False,
            deletable=False,
            editable=True,
            paginate=200,
            maxtextlengths={'budget.comments' : 300},
            user_signature=False
        )
        
        #grid = SQLFORM.grid(db.budget.project_id == session.project, fields = fields, create=False, deletable=True, editable=True, details=False, maxtextlengths={'budget.comments' : 300})
    elif session.project in session.sup_projects:
        redirect(URL('po', 'list'))
    return dict(list=grid)

@auth.requires_login()
def add():
    form = SQLFORM.factory(
        Field('amount', 'double', requires=IS_NOT_EMPTY()),
        Field('comments', 'text')
    )
    if form.process().accepted:
        response.flash = 'form accepted'
        
        po_num = db.budget.insert(project_id=session.project,
                              comments=form.vars.comments,
                              amount=form.vars.amount)
        
        redirect(URL('budget', 'list'))
    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill out the form'
    return dict(form=form)

@auth.requires_login()
def over():
    if not (request.args[0] == None):
        overbudget = '$' + request.args[0]
    else:
        redirect(URL('default', 'index'))
    return dict(over = overbudget)

@auth.requires_login()
def delete():
    budget_id = request.args(0,cast=int)
    db(db.budget.id == budget_id).delete()
    attachments = db(db.attachments.budget_id == budget_id).select()
    deleteattachments(attachments)
    
    db.commit()
    
    redirect(request.vars.source)
