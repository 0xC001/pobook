# -*- coding: utf-8 -*-
#@cache.action(time_expire=300, cache_model=cache.ram, quick='SVP')
@auth.requires_login()
def list():
    
    if not (request.vars.source == None):
        source = request.vars.source
        here = URL(args=request.args, vars=request.vars)
    else:
        redirect(URL('default', 'index'))
        
    
    if  request.args(0) != None:

        category = request.args(0)
        
        commitments=[]
        
        details = {
                'vendor': None,
                'vendor_id': None,
                'commitment': None,
                'commitment_id': None
            }
        
        if category == 'vendor':
            if not (request.args(1, cast=int) == None):
                vendor_id = request.args(1, cast=int)
                query = db.invoices.vendor_id == vendor_id
                details['vendor'] = db(db.vendors.id == vendor_id).select()[0].name
                details['vendor_id'] = vendor_id
            else:
                redirect(source)
                
            if not (len(commitments) > 0):
                response.flash = 'Please create a commitment before entering invoices against one'
            
        elif category == 'commitment':
            if not (request.args(1, cast=int) == None):
                commitment_id = request.args(1, cast=int)
                query = db.invoice_items.commitment_id == commitment_id
                po = db(db.po.id == commitment_id).select()[0]
                details['commitment'] = po.num
                details['commitment_id'] = po.id
                details['vendor'] = db(db.vendors.id == po.vendor_id).select()[0].name
                details['vendor_id'] = po.vendor_id
            else:
                redirect(source)
    else:
        redirect(source)
    
    fields = [
        db.invoices.id,
        db.invoice_items.commitment_id,
        db.po.num,
        db.invoices.num,
        db.invoices.dated,
        db.invoices.date_received,
        db.invoices.date_entered,
        db.invoices.amount_beforeHoldback,
        #db.invoices.amount_pretax,
        db.invoices.amount_total,
        db.invoices.paid_date,
        db.invoices.paid_doc
    ]
    
    headers = {
        'invoices.id': 'ID', 
        'invoice_items.commitment_id': 'ID',
        'po.num': 'Contract No.',
        'invoices.num': 'Invoice No.', 
        'invoices.dated': 'Inv. Date', 
        'invoices.date_received': "Date Rec'd", 
        'invoices.date_entered': 'Date Ent.', 
        'invoices.amount_beforeHoldback': 'Before Holdback Amt.', 
        #'invoices.amount_pretax': 'Pre-tax Amt.', 
        'invoices.amount_total': 'Total', 
        'invoices.paid_date': 'Paid On',
        'invoices.paid_doc': 'Paid With'
    }
    
    grid = []
    
    maxlengths = {
        'invoices.id': 10, 
        'invoice_items.commitment_id': 10,
        'po.num': 100,
        'invoices.num': 100, 
        'invoices.dated': 15, 
        'invoices.date_received': 15, 
        'invoices.date_entered': 15, 
        'invoices.amount_beforeHoldback': 10, 
        'invoices.amount_total': 10, 
        'invoices.paid_date': 15,
        'invoices.paid_doc': 15
    }
    
    left = [
            db.po.on(db.po.id == db.invoice_items.commitment_id),
            db.invoices.on(db.invoices.id == db.invoice_items.invoice_id)
            ]
    
    links = [
        lambda row: A('Edit',_class='button btn btn-default',_href=URL('edit', args=[category, row.invoices.id], vars=dict(source=here))),
        lambda row: A('Files',_class='button btn btn-default',_href=URL('attachments', 'list', args=['invoice',row.invoices.id], vars=dict(source=here))),
        lambda row: A('Delete',_class='button btn btn-default',_href=URL('delete', args=[row.invoices.id], vars=dict(source=here)))
    ]
    grid = SQLFORM.grid(
        query,
        left=left,
        fields = fields,
        links=links,
        create=False,
        details=False,
        deletable=False,
        editable=False,
        maxtextlengths=maxlengths,
        paginate=200,
        orderby=[~db.invoices.num],
        headers=headers,
        user_signature=False
    )

    return dict(list=grid, here=here, back=source, details=details)

@auth.requires_login()
def add():
    if not (request.vars.source == None):
        source = request.vars.source
        here = URL(args=request.args, vars=request.vars)
    else:
        redirect(URL('vendor', 'list'))
    
    vendor_name = None
    commitment_num = None
    
    if not (request.args(0) == None):
        if request.args(0) == 'vendor':
            vendor_id = request.args(1, cast=int)
            commitments = db(db.po.vendor_id == request.args(1, cast=int)).select()
            vendor_name = db(db.vendors.id == vendor_id).select()[0].name
            list = []
            #vendor_id_to_name = dict()
            
            for commitment in commitments:
                list.append(commitment.num)
            #for vendor in vendor:
            #    vendor_id_to_name.update({str(vendor.id) : vendor.name})
                
            form = SQLFORM.factory(
                    #Field('vendor', db.vendors, requires=IS_IN_DB(db,'vendors.id', '%(name)s'), default=vendor_id),
                    Field('commitment', db.po, requires=IS_IN_SET(list), default=None),
                    Field('num', 'string', requires=IS_NOT_EMPTY(), default=None),
                    Field("dated", "date", default=None, requires=[
                        IS_NOT_EMPTY(), 
                        IS_DATE_FORMATTED(request.get_vars.dated)]),
                    Field("date_received", "date", default=None, requires=
                        IS_DATE_FORMATTED(request.get_vars.date_received)),
                    Field("date_entered", "date", default=None, requires=
                        IS_DATE_FORMATTED(request.get_vars.date_entered)),
                    Field("description", "text", notnull=True, default=None),
                    Field("amount_before_holdback", "double", notnull=True, default=0.00),
                    Field("no_holdback", "boolean", default=False),
                    Field("amount_pretax", "double", notnull=True, default=0.00),
                    Field("amount_total", "double", notnull=True, default=0.00, requires=IS_NOT_EMPTY()),
                    Field("paid_date", "date", default=None, requires=
                        IS_DATE_FORMATTED(request.get_vars.paid_date)),
                    Field("paid_doc", "string", default=None)
                    )

        elif request.args(0) == 'commitment':
            po_id = request.args(1, cast=int)
            commitment_item = db(db.item.po_id == po_id).select()[0]
            commitment_item_id = commitment_item.id
            commitment = db(db.po.id == po_id).select()[0]
            commitment_num = commitment.num
            vendor_name = vendor_name = db(db.vendors.id == commitment.vendor_id).select()[0].name
            vendor_id = commitment.vendor_id
            
            form = SQLFORM.factory(
                    #Field('vendor', db.vendors, requires=IS_IN_DB(db,'vendors.id', '%(name)s'), default=None),
                    #Field('commitment', db.po, requires=IS_IN_DB(db,'po.num', '%(num)s'), default=commitment.num),
                    Field('num', 'string', requires=IS_NOT_EMPTY(), default=None),
                    Field("dated", "date", default=None, requires=[
                        IS_NOT_EMPTY(), 
                        IS_DATE_FORMATTED(request.get_vars.dated)]),
                    Field("date_received", "date", default=None, requires=
                        IS_DATE_FORMATTED(request.get_vars.date_received)),
                    Field("date_entered", "date", default=None, requires=
                        IS_DATE_FORMATTED(request.get_vars.date_entered)),
                    Field("description", "text", notnull=True, default=None),
                    Field("amount_before_holdback", "double", notnull=True, default=0.00),
                    Field("no_holdback", "boolean", default=False),
                    Field("amount_pretax", "double", notnull=True, default=0.00),
                    Field("amount_total", "double", notnull=True, default=0.00, requires=IS_NOT_EMPTY()),
                    Field("paid_date", "date", default=None, requires=
                        IS_DATE_FORMATTED(request.get_vars.paid_date)),
                    Field("paid_doc", "string", default=None)
                    )

        else:
            response.flash = 'Not enough arguments in URL, or the wrong ones'
    else:
        redirect(URL('default', 'index'))

    form.vars.updated_on = request.now
    form.vars.updated_by = auth.user.id
    form.vars.locked_user = None
    form.vars.vendor_id = vendor_id
    form.vars.invoice_orig = None
    
    if form.process(onvalidation=adjustCommitment).accepted:
    #if form.process().accepted:
        response.flash = 'form accepted'
        
        if request.args(0) == 'vendor':
            
            invoice = db.invoices.insert(
                                vendor_id=vendor_id,
                                num=form.vars.num,
                                dated=form.vars.dated,
                                date_received=form.vars.date_received,
                                date_entered=form.vars.date_entered,
                                amount_beforeHoldback=form.vars.amount_before_holdback,
                                amount_pretax=form.vars.amount_pretax,
                                amount_total=form.vars.amount_total,
                                paid_date=form.vars.paid_date,
                                paid_doc=form.vars.paid_doc,
                                updated_on=form.vars.updated_on,
                                updated_by=form.vars.updated_by,
                                locked_user=form.vars.locked_user
                                )


            commitment_item_id = db(db.item.po_id == form.vars.commitment).select(db.item.id)[0].id

            item = db.invoice_items.insert(
                                invoice_id=invoice,
                                commitment_id=long(form.vars.commitment),
                                commitment_item_id=commitment_item_id,
                                description=form.vars.description,
                                amount_beforeHoldback=form.vars.amount_before_holdback,
                                amount_pretax=form.vars.amount_pretax,
                                updated_on=form.vars.updated_on,
                                updated_by=form.vars.updated_by,
                                locked_user=form.vars.locked_user
                                )
            
        elif request.args(0) == 'commitment':
            invoice = db.invoices.insert(
                            vendor_id=commitment.vendor_id,
                            num=form.vars.num,
                            dated=form.vars.dated,
                            date_received=form.vars.date_received,
                            date_entered=form.vars.date_entered,
                            amount_beforeHoldback=form.vars.amount_before_holdback,
                            amount_pretax=form.vars.amount_pretax,
                            amount_total=form.vars.amount_total,
                            paid_date=form.vars.paid_date,
                            paid_doc=form.vars.paid_doc,
                            updated_on=form.vars.updated_on,
                            updated_by=form.vars.updated_by,
                            locked_user=form.vars.locked_user
                            )


            commitment_item_id = db(db.item.po_id == commitment.id).select(db.item.id)[0].id

            item = db.invoice_items.insert(
                                invoice_id=invoice,
                                commitment_id=long(commitment.id),
                                commitment_item_id=commitment_item_id,
                                description=form.vars.description,
                                amount_beforeHoldback=form.vars.amount_before_holdback,
                                amount_pretax=form.vars.amount_pretax,
                                updated_on=form.vars.updated_on,
                                updated_by=form.vars.updated_by,
                                locked_user=form.vars.locked_user
                                )
            
        db.commit()
        
        if not (invoice == None):
            redirect(URL('list', args=request.args, vars=dict(source=request.vars.source)))

        else:
            response.flash = 'form cannot be processed because of an internal server error'
        
    elif form.errors:
        response.flash = 'form has errors'
        
    else:
        pass
        
    return dict(form=form, back=source, vendor=vendor_name, commitment=commitment_num)

#http://www.web2pyslices.com/slice/show/1471/sqlformgrid
#https://groups.google.com/forum/#!topic/web2py/Swn_mjFeJGI
@auth.requires_login()
def edit():
    if not (request.vars.source == None):
        source = request.vars.source
        here = URL(args=request.args, vars=request.vars)
    else:
        redirect(URL('vendor', 'list'))
    
    if not (request.args(1) == None):
        invoice_id = request.args(1, cast=int)
    else:
        redirect(source)
    
    vendor_name = None
    commitment_num = None
    commitment_id = None
    
    if not (request.args(0) == None):
        if request.args(0) == 'vendor':
            invoice = db(db.invoices.id == invoice_id).select()
            invoice_item = db(db.invoice_items.invoice_id == invoice_id).select()
            vendor_id = invoice[0].vendor_id
            invoice_orig = invoice[0].num
            commitments = db(db.po.vendor_id == vendor_id).select()
            vendor = db(db.vendors.id == invoice[0].vendor_id).select()
            vendor_name = db(db.vendors.id == vendor_id).select()[0].name
            list = []
            com_num_to_id = dict()
            com_id_to_num = dict()
            vendor_id_to_name = dict()
            
            for commitment in commitments:
                list.append(commitment.num)
                com_num_to_id.update({str(commitment.num) : commitment.id})
                com_id_to_num.update({str(commitment.id) : commitment.num})
            for vendor in vendor:
                vendor_id_to_name.update({str(vendor.id) : vendor.name})

            if invoice_item[0].commitment_id == None:
                commitment_id = str(com_num_to_id[str(list[0])])
            else:
                commitment_id = str(invoice_item[0].commitment_id)

            form = SQLFORM.factory(
                #Field('vendor', db.vendors, requires=IS_IN_DB(db,'vendors.id', '%(name)s'), default=invoice[0].vendor_id),
                Field('commitment', db.invoice_items, requires=IS_IN_SET(list), default=com_id_to_num[commitment_id]),
                Field('num', 'string', requires=IS_NOT_EMPTY(), default=invoice[0].num),
                Field("dated", "date", default=invoice[0].dated, requires=[
                        IS_NOT_EMPTY(),
                        IS_DATE_FORMATTED(request.get_vars.dated)
                    ]),
                Field("date_received", "date", default=invoice[0].date_received, requires=
                     IS_DATE_FORMATTED(request.get_vars.date_received)),
                Field("date_entered", "date", default=invoice[0].date_entered, requires=
                     IS_DATE_FORMATTED(request.get_vars.date_entered)),
                Field("description", "text", notnull=True, default=invoice_item[0].description),
                Field("amount_before_holdback", "double", notnull=True, default=invoice[0].amount_beforeHoldback),
                Field("no_holdback", "boolean", default=False),
                Field("amount_pretax", "double", notnull=True, default=invoice_item[0].amount_pretax),
                Field("amount_total", "double", notnull=True, requires=IS_NOT_EMPTY(), default=invoice[0].amount_total),
                Field("paid_date", "date", default=invoice[0].paid_date, requires=
                     IS_DATE_FORMATTED(request.get_vars.paid_date)),
                Field("paid_doc", "string", default=invoice[0].paid_doc)
                )
            
        elif request.args(0) == 'commitment':
            invoice = db(db.invoices.id == invoice_id).select()
            invoice_item = db(db.invoice_items.invoice_id == invoice_id).select()
            vendor_id = invoice[0].vendor_id
            invoice_orig = invoice[0].num
            commitments = db(db.po.vendor_id == vendor_id).select()
            vendor = db(db.vendors.id == invoice[0].vendor_id).select()
            #vendor_name = db(db.vendors.id == vendor_id).select()[0].name
            list = []
            com_num_to_id = dict()
            com_id_to_num = dict()
            vendor_id_to_name = dict()
            vendor_name = vendor[0].name


            for commitment in commitments:
                list.append(commitment.num)
                com_num_to_id.update({str(commitment.num) : commitment.id})
                com_id_to_num.update({str(commitment.id) : commitment.num})
            for vendor in vendor:
                vendor_id_to_name.update({str(vendor.id) : vendor.name})

            if invoice_item[0].commitment_id == None:
                commitment_id = str(com_num_to_id[str(list[0])])
            else:
                commitment_id = str(invoice_item[0].commitment_id)

            commitment_num = com_id_to_num[str(commitment_id)]

            form = SQLFORM.factory(
                #Field('vendor', db.vendors, requires=IS_IN_DB(db,'vendors.id', '%(name)s'), default=invoice[0].vendor_id),
                Field('commitment', db.invoice_items, requires=IS_IN_SET(list), default=com_id_to_num[commitment_id]),
                Field('num', 'string', requires=IS_NOT_EMPTY(), default=invoice[0].num),
                Field("dated", "date", default=invoice[0].dated, requires=[
                        IS_NOT_EMPTY(), 
                        IS_DATE_FORMATTED(request.get_vars.dated)]),
                Field("date_received", "date", default=invoice[0].date_received, requires=
                     IS_DATE_FORMATTED(request.get_vars.date_received)),
                Field("date_entered", "date", default=invoice[0].date_entered, requires=
                     IS_DATE_FORMATTED(request.get_vars.date_entered)),
                Field("description", "text", notnull=True, default=invoice_item[0].description),
                Field("amount_before_holdback", "double", notnull=True, requires=IS_NOT_EMPTY(), default=invoice_item[0].amount_beforeHoldback),
                Field("no_holdback", "boolean", default=False),
                Field("amount_pretax", "double", notnull=True, requires=IS_NOT_EMPTY(),default=invoice_item[0].amount_pretax),
                Field("amount_total", "double", notnull=True, requires=IS_NOT_EMPTY(), default=invoice[0].amount_total),
                Field("paid_date", "date", default=invoice[0].paid_date, requires=
                     IS_DATE_FORMATTED(request.get_vars.paid_date)),
                Field("paid_doc", "string", default=invoice[0].paid_doc)
                )
            
    form.vars.updated_on = request.now
    form.vars.updated_by = auth.user.id
    form.vars.locked_user = None
    form.vars.vendor_id = vendor_id
    form.vars.invoice_orig = invoice_orig
    vendor = vendor_id_to_name[str(invoice[0].vendor_id)]
    
    if form.process(onvalidation=adjustCommitment).accepted:
        response.flash = 'form accepted'

        if request.args(0) == 'vendor':
            if form.vars.commitment == None:
                if request.vars.commitment != None:
                    commitment = com_num_to_id[request.vars.commitment]
                else:
                    response.flash = 'form has errors'

            invoice[0].update_record(vendor_id=vendor_id)
            invoice_item[0].update_record(commitment_id=form.vars.commitment)
            invoice[0].update_record(num=form.vars.num)
            invoice[0].update_record(dated=form.vars.dated)
            invoice[0].update_record(date_received=form.vars.date_received)
            invoice[0].update_record(date_entered=form.vars.date_entered)
            invoice[0].update_record(amount_beforeHoldback=form.vars.amount_before_holdback)
            invoice[0].update_record(amount_pretax=form.vars.amount_pretax)
            invoice_item[0].update_record(description=form.vars.description)
            invoice_item[0].update_record(amount_beforeHoldback=form.vars.amount_before_holdback)
            invoice_item[0].update_record(amount_pretax=form.vars.amount_pretax)
            invoice[0].update_record(amount_total=form.vars.amount_total)
            invoice[0].update_record(paid_date=form.vars.paid_date)
            invoice[0].update_record(paid_doc=form.vars.paid_doc)
            invoice[0].update_record(updated_on=form.vars.updated_on)
            invoice[0].update_record(updated_by=form.vars.updated_by)
            invoice[0].update_record(locked_user=form.vars.locked_user)
        
        elif request.args(0) == 'commitment':
            if request.vars.commitment != None:
                commitment = com_num_to_id[request.vars.commitment]
            else:
                response.flash = 'form has errors'

            invoice[0].update_record(vendor_id=vendor_id)
            invoice_item[0].update_record(commitment_id=long(commitment))
            invoice[0].update_record(num=form.vars.num)
            invoice[0].update_record(dated=form.vars.dated)
            invoice[0].update_record(date_received=form.vars.date_received)
            invoice[0].update_record(date_entered=form.vars.date_entered)
            invoice[0].update_record(amount_beforeHoldback=form.vars.amount_before_holdback)
            invoice[0].update_record(amount_pretax=form.vars.amount_pretax)
            invoice_item[0].update_record(description=form.vars.description)
            invoice_item[0].update_record(amount_beforeHoldback=form.vars.amount_before_holdback)
            invoice_item[0].update_record(amount_pretax=form.vars.amount_pretax)
            invoice[0].update_record(amount_total=form.vars.amount_total)
            invoice[0].update_record(paid_doc=form.vars.paid_doc)
            invoice[0].update_record(paid_doc=form.vars.paid_doc)
            invoice[0].update_record(updated_on=form.vars.updated_on)
            invoice[0].update_record(updated_by=form.vars.updated_by)
            invoice[0].update_record(locked_user=form.vars.locked_user)
            
        db.commit()
        redirect(source)

    elif form.errors:
        response.flash = 'form has errors'

    else:
        pass

    return dict(form = form, back=source, vendor=vendor_name, commitment=commitment_num)


@auth.requires_login()
def delete():
    invoice_id = request.args(0,cast=int)
    db(db.invoices.id == invoice_id).delete()
    attachments = db(db.attachments.invoice_id == invoice_id).select()
    deleteattachments(attachments)
    
    db.commit()
    
    redirect(request.vars.source)
