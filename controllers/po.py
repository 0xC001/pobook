# -*- coding: utf-8 -*-
# try something like

# -*- coding: utf-8 -*-
# try something like
#@cache.action(time_expire=300, cache_model=cache.ram, quick='SVP')
@auth.requires_login()
def list():
    source = request.vars.source
    here = URL(args=request.args, vars=request.vars)
    
    headers = {
        'po.id': 'ID', 
        'project.num': 'Project No.',
        'project.name': 'Project Name',
        'po.num': 'Contract No.', 
        'vendors.name': 'Vendor Name', 
        'po.scope': 'Scope/Title', 
        'item.description': 'Description', 
        'po.comments': 'Comments', 
        'item.amount': 'Amount', 
        'invoices.amount_beforeHoldback': 'Invoiced', 
        #'invoice_items.amount_pretax': 'Invoiced'
    }
    
    if len(request.args) > 0:
        category = request.args(0)
        asset = request.args(1, cast=long)

        if category == 'vendor':
            vendor_id = asset
            vendor = db(db.vendors.id == vendor_id).select()[0].name
            project_id = None
            project_name = None
            session_project = None
            query = db.po.vendor_id == vendor_id
            user_project = None

        elif category == 'project': 
            vendor_id = None
            vendor = None
            project_id = asset
            project = db(db.project.id == project_id).select(db.project.id, db.project.num, db.project.name).as_list()
            project_name = project[0]['name']
            session_project = project[0]
            query = db.po.project_id == asset
            if (session_project in session.projects):
                user_project = True
            else:
                user_project = False

        else:
            redirect(URL('default', 'index'))
        
        
        invoice_items = db(db.invoice_items).select()
#        projects = db(db.project).select()
#        project_id_to_number = {}
        
#        for project in projects:
#            project_id_to_number[str(project.id)] = project.num
        
        left = [
            db.po.on(db.item.po_id==db.po.id), 
            db.vendors.on(db.vendors.id==db.po.vendor_id),
            db.project.on(db.project.id==db.po.project_id)
        ]
        
        pmfields = [db.po.id, db.project.num, db.project.name, db.po.num, db.vendors.name, db.po.scope, db.item.description, db.po.comments, db.item.amount]
        supfields = [db.po.id, db.project.num, db.project.name, db.po.num, db.vendors.name, db.po.closed, db.item.description, db.po.comments, db.item.amount]
        allfields = [db.po.id, db.project.num, db.project.name, db.po.num, db.vendors.name, db.po.scope, db.item.description, db.po.comments, db.item.amount]
        
        grid = []
        
        links_pm = [
            dict(header='Invoiced', body=lambda row: DIV(('${:,.2f}'.format(getinvoicetotal(row.po.id, invoice_items))), _style='text-align: right;')),
            dict(header='Balance', body=lambda row: DIV(('${:,.2f}'.format(row.item.amount - getinvoicetotal(row.po.id, invoice_items))), _style='text-align: right;')),
            dict(header='Inv', body=lambda row: A('Invoices',_class='button btn btn-default',_href=URL('invoice', 'list', args=['commitment', row.po.id], vars=dict(source=here)))),
            dict(header='Edit', body=lambda row: A('Edit',_class='button btn btn-default',_href=URL('edit', args=row.po.id, vars=dict(source=here)))),
            dict(header='Attach', body=lambda row: A('Files',_class='button btn btn-default',_href=URL('attachments', 'list', args=['commitment',row.po.id], vars=dict(source=here)))),
            dict(header='Del', body=lambda row: A('Delete',_class='button btn btn-default',_href=URL('delete', args=row.po.id, vars=dict(source=here))))
        ]

        links_super = [
            dict(header='Invoiced', body=lambda row: DIV(('${:,.2f}'.format(getinvoicetotal(row.po.id, invoice_items))), _style='text-align: right;')),
            dict(header='Balance', body=lambda row: DIV(('${:,.2f}'.format(row.item.amount - getinvoicetotal(row.po.id, invoice_items))), _style='text-align: right;')),
            dict(header='Attach', body=lambda row: A('Files',_class='button btn btn-default',_href=URL('attachments', 'list', args=['commitment',row.po.id], vars=dict(source=here)))),
            dict(header='Edit', body=lambda row: A('Edit',_class='button btn btn-default',_href=URL('edit', args=row.po.id, vars=dict(source=here))))
        ]
        
        links_all = [
            dict(header='Invoiced', body=lambda row: DIV(('${:,.2f}'.format(getinvoicetotal(row.po.id, invoice_items))), _style='text-align: right;')),
            dict(header='Balance', body=lambda row: DIV(('${:,.2f}'.format(row.item.amount - getinvoicetotal(row.po.id, invoice_items))), _style='text-align: right;')),
            dict(header='Inv', body=lambda row: A('Invoices',_class='button btn btn-default',_href=URL('invoice', 'list', args=['commitment', row.po.id], vars=dict(source=here)))),
            dict(header='Attach', body=lambda row: A('Files',_class='button btn btn-default',_href=URL('attachments', 'list', args=['commitment',row.po.id], vars=dict(source=here))))
        ]
        
    else:
        if session.project > 0:
            query = db.po.project_id == session.project
        else:
            redirect(URL('vendor', 'list'))
        
        vendor = None
        vendor_id = None
        project_id = session.project
        project = db(db.project.id == project_id).select(db.project.id, db.project.num, db.project.name).as_list()
        project_name = project[0]['name']
        session_project = project[0]
        if (session_project in session.projects):
            user_project = True
        else:
            user_project = False
        invoice_items = db(db.invoice_items).select()
        
        pmfields = [db.po.id, db.po.num, db.vendors.name, db.po.scope, db.item.description, db.po.comments, db.item.amount]
        supfields = [db.po.id, db.po.num, db.vendors.name, db.po.closed, db.item.description, db.po.comments, db.item.amount]
        allfields = [db.po.id, db.po.num, db.vendors.name, db.po.scope, db.item.description, db.po.comments, db.item.amount]
        left = [
            db.po.on(db.item.po_id==db.po.id), 
            db.vendors.on(db.vendors.id==db.po.vendor_id)
        ]
        
        grid = []
        
        links_pm = [
            dict(header='Invoiced', body=lambda row: DIV(('${:,.2f}'.format(getinvoicetotal(row.po.id, invoice_items))), _style='text-align: right;')),
            dict(header='Balance', body=lambda row: DIV(('${:,.2f}'.format(row.item.amount - getinvoicetotal(row.po.id, invoice_items))), _style='text-align: right;')),
            dict(header='Inv', body=lambda row: A('Invoices',_class='button btn btn-default',_href=URL('invoice', 'list', args=['commitment', row.po.id], vars=dict(source=here)))),
            dict(header='Edit', body=lambda row: A('Edit',_class='button btn btn-default',_href=URL('edit', args=row.po.id, vars=dict(source=here)))),
            dict(header='Attach', body=lambda row: A('Files',_class='button btn btn-default',_href=URL('attachments', 'list', args=['commitment',row.po.id], vars=dict(source=here)))),
            dict(header='Del', body=lambda row: A('Delete',_class='button btn btn-default',_href=URL('delete', args=row.po.id, vars=dict(source=here))))
        ]

        links_super = [
            dict(header='Invoiced', body=lambda row: DIV(('${:,.2f}'.format(getinvoicetotal(row.po.id, invoice_items))), _style='text-align: right;')),
            dict(header='Balance', body=lambda row: DIV(('${:,.2f}'.format(row.item.amount - getinvoicetotal(row.po.id, invoice_items))), _style='text-align: right;')),
            dict(header='Invoices', body=lambda row: A('Invoices',_class='button btn btn-default',_href=URL('invoice', 'list', args=['commitment', row.po.id], vars=dict(source=here)))),
            dict(header='Files', body=lambda row: A('Files',_class='button btn btn-default',_href=URL('attachments', 'list', args=['commitment',row.po.id], vars=dict(source=here)))),
            dict(header='Edit', body=lambda row: A('Edit',_class='button btn btn-default',_href=URL('edit', args=row.po.id, vars=dict(source=here))))
        ]
        
        links_all = [
            dict(header='Invoiced', body=lambda row: DIV(('${:,.2f}'.format(getinvoicetotal(row.po.id, invoice_items))), _style='text-align: right;')),
            dict(header='Balance', body=lambda row: DIV(('${:,.2f}'.format(row.item.amount - getinvoicetotal(row.po.id, invoice_items))), _style='text-align: right;')),
            dict(header='Inv', body=lambda row: A('Invoices',_class='button btn btn-default',_href=URL('invoice', 'list', args=['commitment', row.po.id], vars=dict(source=here)))),
            dict(header='Attach', body=lambda row: A('Files',_class='button btn btn-default',_href=URL('attachments', 'list', args=['commitment',row.po.id], vars=dict(source=here))))
        ]
    
    if (session.pm_projects == None) and (session.sup_projects == None):
        grid = SQLFORM.grid(
                query, 
                left=left, 
                fields = allfields, 
                links=links_all,
                headers=headers,
                create=False, 
                details=False, 
                deletable=False, 
                editable=False, 
                maxtextlengths={'po.num' : 100, 'item.description' : 50, 'vendor.name' : 100, 'po.comments' : 100, 'project.name' : 100, 'po.scope' : 100}, 
                paginate=200, 
                orderby=[~db.po.num],
                user_signature=False
            )
    if not (session.pm_projects == None):
        if session.project in session.pm_projects:
            grid = SQLFORM.grid(
                query, 
                left=left, 
                fields = pmfields, 
                links=links_pm,
                headers=headers,
                create=False, 
                details=False, 
                deletable=False, 
                editable=False, 
                maxtextlengths={'po.num' : 100, 'item.description' : 50, 'vendor.name' : 100, 'po.comments' : 100, 'project.name' : 100, 'po.scope' : 100}, 
                paginate=200, 
                orderby=[~db.po.num],
                user_signature=False
            )
    if not (session.sup_projects == None):
        if not (session.sup_projects == None):
            if session.project in session.sup_projects:
                grid = SQLFORM.grid(
                    query, 
                    left=left, 
                    fields = supfields, 
                    links=links_super,
                    headers=headers,
                    create=False, 
                    details=False, 
                    deletable=False, 
                    editable=False, 
                    maxtextlengths={'po.num' : 100, 'item.description' : 50, 'vendor.name' : 100, 'po.comments' : 100, 'project.name' : 100, 'po.scope' : 100}, 
                    paginate=200, 
                    orderby=[~db.po.num],
                    user_signature=False
                )
                
    if session.project > 0:
            session.budget_total = getbudget(session.project)
            session.budget_spent = getpototal(session.project)
            
    return dict(grid=grid, vendor=vendor, user_project = user_project, project=session_project, project_name=project_name, project_id=project_id, vendor_id=vendor_id, here=here, back=source)

@auth.requires_login()
def add():
    source = request.vars.source
    here = URL(args=request.args, vars=request.vars)
    
    form = SQLFORM.factory(
                Field('vendor', db.vendors, requires=IS_IN_DB(db,'vendors.id', '%(name)s')),
                Field("holdback", "integer", default=None),
                Field("form_c", "date", default=None, requires=IS_DATE_FORMATTED(request.get_vars.form_c)),
                Field("expired", "date", default=None, requires=IS_DATE_FORMATTED(request.get_vars.expired)),
                Field('closed','date', default=None, requires=IS_DATE_FORMATTED(request.get_vars.closed)),
                Field('scope', 'text'),
                Field('description', 'text'),
                Field('comments', 'text'),
                Field('amount', 'double')
    )
    
    po_last = 0
    if session.project == None:
        redirect(URL('default', 'index'))

    project = db(db.project.id == session.project).select(db.project.last_po, db.project.po_prefix)
    if project == None:
        project = []
        redirect(URL('default', 'index'))

    po_last = project[0].last_po
    prefix = project[0].po_prefix
    bal = getbudgetbal(session.project)
 
    if (po_last is None):
        po_last = 0
    po_last = po_last + 1

    po_number = generatepo(prefix, po_last)
    
    if form.process().accepted:
        response.flash = 'form accepted'
           
        newbal = bal - float(form.vars.amount)
        if newbal < 0:
            newbal = newbal * -1
            redirect(URL('budget', 'over', args=[str(newbal)]))
        
        db(db.project.id == session.project).update(last_po = po_last)
    
        po_num = db.po.insert(
                            project_id=session.project,
                            num=po_number,
                            vendor_id=form.vars.vendor,
                            comments=form.vars.comments,
                            holdback=form.vars.holdback,
                            form_c=form.vars.form_c,
                            expired=form.vars.expired,
                            closed=form.vars.closed,
                            scope=form.vars.scope,
                            updated_on=request.now,
                            updated_by=session.auth.user.id
        )


        item_num = db.item.insert(po_id=po_num,
                          description=form.vars.description,
                          amount=form.vars.amount)
        db.commit()
        
        if session.project > 0:
            session.budget_total = getbudget(session.project)
            session.budget_spent = getpototal(session.project)
        redirect(URL('number', vars=dict(number=po_number)))

    elif form.errors:
        response.flash = 'form has errors'
    else:
        pass
        
    return dict(form=form, po=po_number, back=source)

#http://www.web2pyslices.com/slice/show/1471/sqlformgrid
#https://groups.google.com/forum/#!topic/web2py/Swn_mjFeJGI
@auth.requires_login()
def edit():
    source = request.vars.source
    here = URL(args=request.args, vars=request.vars)
    
    reqpo = request.args[0]
    
    pos = db(db.po.id == reqpo).select()
    items = db(db.item.po_id == reqpo).select()
    vendor = db(db.vendors.id == pos[0].vendor_id).select()
    
    po_number = pos[0].num
    
    if not (session.pm_projects == None):
        if session.project in session.pm_projects:
            
            form = SQLFORM.factory(
                Field('vendor', db.vendors, default=vendor[0].id, requires=IS_IN_DB(db,'vendors.id', '%(name)s')),
                Field('num', 'string', default=pos[0].num, notnull=True),
                Field("holdback", "integer", default=pos[0].holdback, notnull=True),
                Field("form_c", "date", default=pos[0].form_c, requires=IS_DATE_FORMATTED(request.get_vars.form_c)),
                Field("expired", "date", default=pos[0].expired, requires=IS_DATE_FORMATTED(request.get_vars.expired)),
                Field('closed','date', default=pos[0].closed, requires=IS_DATE_FORMATTED(request.get_vars.closed)),
                Field('scope', 'text', default=pos[0].scope),
                Field('description', 'text', default=items[0].description),
                Field('comments', 'text', default=pos[0].comments),
                Field('amount', 'double', default=items[0].amount, notnull=True)
            )

            if not (session.pm_projects == None):
                if session.project in session.pm_projects:
                    data = db(db.po.id == reqpo).select()
                elif not (session.sup_projects == None):
                    #redirect(URL('default', 'index'))
                    redirect(source)

            if form.accepts(request.vars, session):
                pos[0].update_record(vendor_id=form.vars.vendor)
                pos[0].update_record(num=form.vars.num)
                pos[0].update_record(holdback=form.vars.holdback)
                pos[0].update_record(form_c=form.vars.form_c)
                pos[0].update_record(expired=form.vars.expired)
                pos[0].update_record(closed=form.vars.closed)
                pos[0].update_record(scope=form.vars.scope)
                pos[0].update_record(comments=form.vars.comments)
                items[0].update_record(description=form.vars.description)
                items[0].update_record(amount=form.vars.amount)
                db.commit()
                #redirect(URL('po', 'list'))
                redirect(source)
                
            elif form.errors:
                response.flash = 'form has errors'
                
            else:
                pass
                
            return dict(form = form, po=po_number, back=source)

    if not (session.sup_projects == None):
        if session.project in session.sup_projects:
            pos = db(db.po.id == reqpo).select()
            items = db(db.item.po_id == reqpo).select()

            form = SQLFORM.factory(
                        Field('vendor', db.vendors, default=vendor[0].name, requires=IS_IN_DB(db,'vendors.id', '%(name)s')),
                        Field('comments', 'text', default=pos[0].comments),
                        Field('description','text', default=items[0].description, notnull=True)
            )
            
            if not (session.sup_projects == None):
                if session.project in session.sup_projects:
                    data = db(db.po.id == reqpo).select()
                elif not (session.sup_projects == None):
                    #redirect(URL('default', 'index'))
                    redirect(source)

            if form.accepts(request.vars, session):
                pos[0].update_record(vendor_id=form.vars.vendor)
                pos[0].update_record(comments=form.vars.comments)
                items[0].update_record(description=form.vars.description)
                db.commit()
                #redirect(URL('po', 'list'))
                redirect(source)
            elif form.errors:
                response.flash = 'form has errors'
            else:
                pass
                
            return dict(form = form, po=po_number, back=source)
        
        else:
            #redirect(URL('po', 'list'))
            redirect(source)
            
    else:
        #redirect(URL('po', 'list'))
        redirect(source)
        
    return dict()


@auth.requires_login()
def delete():
    po_id = request.args(0,cast=int)
        
    db(db.po.id == po_id).delete()
    attachments = db(db.attachments.commitment_id == po_id).select()
    deleteattachments(attachments)
    
    db.commit()
    
    redirect(request.vars.source)

@auth.requires_login()
def number():
    return dict(number=request.vars.number)
