# -*- coding: utf-8 -*-
#@cache.action(time_expire=300, cache_model=cache.ram, quick='SVP')
@auth.requires_login()
def list():
    source = request.vars.source
    here = URL(args=request.args, vars=request.vars)

    fields = [
        db.vendors.id,
        db.vendors.num,
        db.vendors.name,
        db.vendors.contractor_check_expiry,
        db.vendors.comments
    ]
    
    headers = {
        'vendors.id': 'ID', 
        'vendors.num': 'Vendor No.',
        'vendors.name': 'Vendor Name',
        'po.contractor_check_expiry': 'Contractor Check Exp.', 
        'vendors.comments': 'Comments', 
    }
    
    grid = []
    
    links = [
        dict(header='Committed', body=lambda row: DIV(('${:,.2f}'.format(getvendorcommitmenttotal(row.id))), _style='text-align: right;')),
        dict(header='Invoiced', body=lambda row: DIV(('${:,.2f}'.format(getvendorinvoicetotal(row.id))), _style='text-align: right;')),
        dict(header='Balance', body=lambda row: DIV(('${:,.2f}'.format(getvendorcommitmenttotal(row.id) - getvendorinvoicetotal(row.id))), _style='text-align: right;')),
        dict(header='Commit', body=lambda row: A('Commitments',_class='button btn btn-default',_href=URL('po', 'list', args=['vendor', row.id], vars=dict(source=here)))),
        dict(header='Inv', body=lambda row: A('Invoices',_class='button btn btn-default',_href=URL('invoice', 'list', args=['vendor', row.id], vars=dict(source=here)))),
        dict(header='Edit', body=lambda row: A('Edit',_class='button btn btn-default',_href=URL('edit', args=row.id, vars=dict(asset=row.id, source=here)))),
        dict(header='Attach', body=lambda row: A('Files',_class='button btn btn-default',_href=URL('attachments', 'list', args=['vendor', row.id], vars=dict(source=here)))),
        dict(header='Del', body=lambda row: A('Delete',_class='button btn btn-default',_href=URL('delete', args=row.id, vars=dict(asset=row.id, source=here))))
    ]

    grid = SQLFORM.grid(
        db.vendors,
        fields = fields,
        links=links,
        create=False,
        details=False,
        deletable=False,
        editable=False,
        paginate=200,
        maxtextlengths={'vendors.name' : 100, 'vendors.comments' : 100},
        headers = headers,
        orderby=[~db.vendors.num]
    )


    #grid = SQLFORM.grid(db.vendors)
            
    return dict(list=grid)

@auth.requires_login()
def add():
    source = request.vars.source
    form = SQLFORM.factory(
        Field('num', 'string', default=None, requires=IS_NOT_EMPTY()),
        Field('name', 'string', default=None, requires=IS_NOT_EMPTY()),
        Field('address1', 'string', default=None),
        Field('address2', 'string', default=None),
        Field('address3', 'string', default=None),
        Field('city', 'string', default=None),
        Field('region', 'string', default=None),
        Field('country', 'string', default=None),
        Field('post_code', 'string', default=None),
        Field('contractor_check_expiry', 'date', default=None, requires=IS_DATE_FORMATTED(request.get_vars.contractor_check_expiry)),
        Field('link', 'string', default=None),
        Field('comments', 'text', default=None)
        )


    form.vars.contractor_check_id = None
    form.vars.updated_on = request.now
    form.vars.updated_by = auth.user.id
    form.vars.locked_user = None


    if form.process().accepted:
        response.flash = 'form accepted'
        vendor = db.vendors.insert(
                            num=form.vars.num,
                            name=form.vars.name,
                            address1=form.vars.address1,
                            address2=form.vars.address2,
                            address3=form.vars.address3,
                            city=form.vars.city,
                            region=form.vars.region,
                            country=form.vars.country,
                            post_code=form.vars.post_code,
                            contractor_check_expiry=form.vars.contractor_check_expiry,
                            link=form.vars.link,
                            comments=form.vars.comments,
                            updated_on=form.vars.updated_on,
                            updated_by=form.vars.updated_by
                            )

        db.commit()
        
        if not (vendor == None):
            redirect(URL('list'))

        else:
            response.flash = 'form cannot be processed because of an internal server error'
        
    elif form.errors:
        response.flash = 'form has errors'
        
    else:
        pass
        
    return dict(form=form, back=source)

#http://www.web2pyslices.com/slice/show/1471/sqlformgrid
#https://groups.google.com/forum/#!topic/web2py/Swn_mjFeJGI
@auth.requires_login()
def edit():
    source = request.vars.source
    
    if not (request.args[0] == None):
        vendor_id = request.args[0]
    else:
        redirect(source)
    
    vendor = db(db.vendors.id == vendor_id).select()
    
    form = SQLFORM.factory(
        Field('num', 'string', default=vendor[0].num, requires=IS_NOT_EMPTY()),
        Field('name', 'string', default=vendor[0].name, requires=IS_NOT_EMPTY()),
        Field('address1', 'string', default=vendor[0].address1),
        Field('address2', 'string', default=vendor[0].address2),
        Field('address3', 'string', default=vendor[0].address3),
        Field('city', 'string', default=vendor[0].city),
        Field('region', 'string', default=vendor[0].region),
        Field('country', 'string', default=vendor[0].country),
        Field('post_code', 'string', default=vendor[0].post_code),
        Field('contractor_check_expiry', 'date', default=vendor[0].contractor_check_expiry, requires=IS_DATE_FORMATTED(request.get_vars.contractor_check_expiry)),
        Field('link', 'string', default=vendor[0].link),
        Field('comments', 'text', default=vendor[0].comments)
        )
    
    form.vars.contractor_check_id = None
    form.vars.updated_on = request.now
    form.vars.updated_by = auth.user.id
    form.vars.locked_user = None


    if form.accepts(request.vars, session):
        response.flash = 'form accepted'
        vendor[0].update_record(num=form.vars.num)
        vendor[0].update_record(name=form.vars.name)
        vendor[0].update_record(address1=form.vars.address1)
        vendor[0].update_record(address2=form.vars.address2)
        vendor[0].update_record(address3=form.vars.address3)
        vendor[0].update_record(city=form.vars.city)
        vendor[0].update_record(region=form.vars.region)
        vendor[0].update_record(country=form.vars.country)
        vendor[0].update_record(post_code=form.vars.post_code)
        vendor[0].update_record(contractor_check_expiry=form.vars.contractor_check_expiry)
        vendor[0].update_record(link=form.vars.link)
        vendor[0].update_record(comments=form.vars.comments)
        vendor[0].update_record(updated_on=form.vars.updated_on)
        vendor[0].update_record(updated_by=form.vars.updated_by)
        vendor[0].update_record(locked_user=form.vars.locked_user)
        db.commit()
        redirect(URL('list'))

    elif form.errors:
        response.flash = 'form has errors'

    else:
        pass

    return dict(form = form, back=source)


@auth.requires_login()
def delete():
    vendor_id = request.args(0,cast=int)
        
    db(db.vendors.id == vendor_id).delete()
    attachments = db(db.attachments.vendor_id == vendor_id).select()
    deleteattachments(attachments)
    
    db.commit()
    
    redirect(request.vars.source)
